package com.smartinsoft.kpsskazan;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class showkat extends Activity {
    String keys;
    PDFView pdf;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showkat);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        MobileAds.initialize(this,
                "ca-app-pub-1312048647642571~5507586644");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        pdf = (PDFView)findViewById(R.id.pdfView);
        Bundle extras = getIntent().getExtras();
        final int sira1 = extras.getInt("sira1");
        final int sira2 = extras.getInt("sira2");
        int sira3 = extras.getInt("sira3");
        sira3+=1;
        switch (sira2)
        {
            case 0:{
                 keys = "t" +sira3;
                break;}
            case 1:{
                 keys = "m" +sira3;
            }
            case 2:{
                keys = "g" +sira3;
            }
            case 3:{
                keys = "ta" +sira3;
            }
            case 4:{
                keys = "c" +sira3;
            }
            case 5:{
                keys = "v" +sira3;
            }
        }
        String asset=keys+".pdf";
        pdf.fromAsset(String.valueOf(asset)).load();
        pdf.recycle();
    }
}
