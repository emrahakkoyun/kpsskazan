package com.smartinsoft.kpsskazan;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class Listes extends AppCompatActivity {
    String[] Selectgelen;
    ListView listView;
    int nerde=0,who;
    String[] konu = new String[]{"Türkçe", "Matematik", "Geometri",
            "Tarih", "Coğrafya", "Vatandaşlık"};
    String[] turkce = new String[]{
            "Kelime Anlamı",
            "Cümle Anlatımı",
            "Paragraf",
            "Dil Bilgisi Ekler",
            "Sözcüğün Yapısı",
            "Cümlenin Öğeleri",
            "Vurgu",
            "Fiil Çatısı",
            "Cümle Çeşitleri",
            "Anlatım Bozukluğu",
            "Ses Bilgisi",
            "Noktalama İşaretleri",
            "Yazım Kuralları"
    };
    String[] matematik = new String[]{
            "Temel Kavramlar",
            "Sayı Sistemleri",
            "Bölme-Bölünebilme",
            "Ebob-Ekok",
            "Rasyonel Sayılar",
            "Mutlak Değer",
            "Üslü Sayılar",
            "Köklü Sayılar",
            "Çarpanlarına Ayırma",
            "Oran-Orantı",
            "Birinci Dereceden Denklemler",
            "Problemler",
            "Kümeler",
            "Kartezyen Çarpımı ve Bağıntı",
            "Fonksiyonlar",
            "İşlem",
            "Modüler Aritmatik"
    };
    String[] geometri = new String[]{
            "Geometrik Kavramlar",
            "Üçgenler",
            "Özel Üçgenler",
            "Üçgende Alan",
            "Açıortay",
            "Kenarortay",
            "Üçgende Benzerlik",
            "Üçgende Açı Kenar Bağıntıları",
            "Dörtgenler",
            "Çokgenler",
            "Paralelkenar",
            "Eşkenar Dörtgen",
            "Dikdörtgen",
            "Kare",
            "Deltoid",
            "Yamuk",
            "Çemberde Açı",
            "Çemberde Uzunluk",
            "Çemberde Benzerlik",
            "Dairede Alan ve Uzunluk"
    };
    String[] tarih = new String[]{
            "KPSS Tarih Bilimine Giriş",
            "İslam Tarih",
            "Türk İslam Tarihi",
            "Türk İslam Devletleri",
            "Türk-İslam Devletlerinde Kültür ve Medeniyet",
            "Orta Çağda Avrupa",
            "Anadolu’daki İlk Beylikler",
            "Beylikler Dönemi",
            "Osmanlı Devleti Tarih",
            "Osmanlı Kuruluş Devri",
            "Osmanlı Yükseliş Devri",
            "Yeni Çağda Avrupadaki Gelişmeler",
            "Osmanlı Duraklama Devri",
            "Osmanlı Gerileme Devri",
            "Yakın Çağda Avrupa",
            "Balkan Savaşları",
            "1.Dünya Savaşı",
            "Mondros Ateşkes Antlaşması",
            "Kurtuluş Savaşı Hazırlık Dönemi",
            "Lozan Barış Antlaşması"

    };
    String[] cografya =new String[]{
           "Ülkeler Coğrafyası",
            "Coğrafi Konum",
            "Dünyanın Şekli ve Hareketleri",
            "Harita Bilgisi",
            "İklim",
            "İç ve Dış Kuvvetler",
            "Türkiye’nin Beşeri Özellikleri",
            "Türkiyenin Ekonomik Coğrafyası",
            "Türkiye’nin Coğrafi Bölgeleri"


    };
    String[] vatandaslik =new String[]{
            "KPSS Vatandaşlık Toplumsal Yaşam",
            "KPSS Temel Toplumsal Kurum Olarak Aile",
            "KPSS Eğitim ve Öğretim",
            "KPSS Millet",
            "KPSS Siyaset",
            "KPSS Demokrasi",
            "KPSS Ekonomi",
            "KPSS Anayasa",
            "KPSS Temel Hak ve Ödevimiz",
            "KPSS Milli Güvenlik Bilgisi",
            "KPSS Türkiye’nin İştirak Ettiği Milletlerarası Organizasyonlar"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        listView = (ListView) findViewById(R.id.listview);
        Selectgelen=konu;
       sirala();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if(nerde==0){
                    who=position;
                switch (position) {
                    case 0: {
                        Selectgelen=turkce;
                        break;
                    }
                    case 1: {
                        Selectgelen=matematik;
                        break;
                    }
                    case 2: {
                        Selectgelen=geometri;
                        break;
                    }
                    case 3: {
                        Selectgelen=tarih;
                        break;
                    }

                    case 4: {
                        Selectgelen=cografya;
                        break;
                    }

                    case 5: {
                        Selectgelen=vatandaslik;
                        break;
                    }
                }}
                else{
                  Intent in  = new Intent(Listes.this,showkat.class);
                    in.putExtra("sira2",who);
                    in.putExtra("sira3",position);
                  startActivity(in);
                }
                nerde=1;
                sirala();
            }
        });
    }

    public void sirala()
    {

        final ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < Selectgelen.length; ++i) {
            list.add(Selectgelen[i]);
        }
        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
    }

}